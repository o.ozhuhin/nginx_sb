FROM nginx
RUN mkdir /usr/share/nginx/wordpress
COPY /config /etc/nginx/conf.d/
COPY /www /usr/share/nginx/wordpress
RUN rm -f /etc/nginx/conf.d/default.conf

EXPOSE 8080:80


